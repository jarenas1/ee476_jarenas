* File: /gscratch/ece/courses/476/jarenas/jarenas_git/cad6/netlist/PEX/OUT_MUX_BUF_16B.pex.netlist.OUT_MUX_BUF_16B.pxi
* Created: Fri Dec  3 16:13:59 2021
* 
cc_1 CTRL_2_BUF<1> ARITHMETIC_OUT<0> 0.00797999f
cc_2 CTRL_2_BUF<1> CTRL_2_BUF<0> 0.02788f
cc_3 CTRL_2_BUF<1> LOGIC_OUT<0> 0.00676047f
cc_4 CTRL_2_BUF<1> CTRL_2_BUF<2> 0.0279411f
cc_5 CTRL_2_BUF<1> ARITHMETIC_OUT<1> 0.0395978f
cc_6 CTRL_2_BUF<1> LOGIC_OUT<1> 0.387056f
cc_7 CTRL_2_BUF<1> ALU_PRE<0> 0.0649196f
cc_8 CTRL_2_BUF<1> NET3<15> 0.0245011f
cc_9 CTRL_2_BUF<1> ALU_OUT<0> 0.0202126f
cc_10 CTRL_2_BUF<1> ALU_PRE<1> 0.11927f
cc_11 CTRL_2_BUF<1> NET3<14> 3.98891e-19
cc_12 CTRL_2_BUF<1> VSS! 0.133164f
cc_13 CTRL_2_BUF<1> VDD! 0.0590196f
cc_14 CTRL_2_BUF<1> XI0<1>/X1 1.98567f
cc_15 CTRL_2_BUF<1> XI0<1>/Z_NEG 0.0171039f
cc_16 CTRL_2_BUF<1> XI4/Z_NEG 0.318211f
cc_17 CTRL_2_BUF<1> XI0<0>/X1 0.0202366f
cc_18 CTRL_2_BUF<1> XI0<0>/Z_NEG 0.0270088f
cc_19 CTRL_2_BUF<1> XI3/Z_NEG 0.408473f
cc_20 ARITHMETIC_OUT<0> CTRL_2_BUF<0> 0.0395978f
cc_21 ARITHMETIC_OUT<0> ALU_PRE<0> 0.0101137f
cc_22 ARITHMETIC_OUT<0> VSS! 0.00499882f
cc_23 ARITHMETIC_OUT<0> VDD! 0.0167587f
cc_24 ARITHMETIC_OUT<0> XI0<0>/X1 0.437839f
cc_25 ARITHMETIC_OUT<0> XI0<0>/Z_NEG 0.441849f
cc_26 CTRL_2_BUF<0> LOGIC_OUT<0> 0.387056f
cc_27 CTRL_2_BUF<0> ALU_PRE<0> 0.11927f
cc_28 CTRL_2_BUF<0> NET3<15> 3.98891e-19
cc_29 CTRL_2_BUF<0> VSS! 0.043197f
cc_30 CTRL_2_BUF<0> VDD! 0.0512777f
cc_31 CTRL_2_BUF<0> XI0<0>/X1 1.98567f
cc_32 CTRL_2_BUF<0> XI0<0>/Z_NEG 0.0171039f
cc_33 CTRL_2_BUF<0> XI3/Z_NEG 0.318181f
cc_34 LOGIC_OUT<0> ALU_PRE<0> 0.0133542f
cc_35 LOGIC_OUT<0> NET3<15> 7.05017e-19
cc_36 LOGIC_OUT<0> VSS! 0.00526763f
cc_37 LOGIC_OUT<0> VDD! 0.0204938f
cc_38 LOGIC_OUT<0> XI0<0>/X1 0.297816f
cc_39 LOGIC_OUT<0> XI0<0>/Z_NEG 0.00144576f
cc_40 CTRL_2_BUF<2> ARITHMETIC_OUT<1> 0.00797999f
cc_41 CTRL_2_BUF<2> LOGIC_OUT<1> 0.00676047f
cc_42 CTRL_2_BUF<2> CTRL_2_BUF<3> 0.0279411f
cc_43 CTRL_2_BUF<2> ARITHMETIC_OUT<2> 0.0395978f
cc_44 CTRL_2_BUF<2> LOGIC_OUT<2> 0.387056f
cc_45 CTRL_2_BUF<2> ALU_PRE<1> 0.0649196f
cc_46 CTRL_2_BUF<2> NET3<14> 0.0245011f
cc_47 CTRL_2_BUF<2> ALU_OUT<1> 0.0202126f
cc_48 CTRL_2_BUF<2> ALU_PRE<2> 0.11927f
cc_49 CTRL_2_BUF<2> NET3<13> 3.98891e-19
cc_50 CTRL_2_BUF<2> VSS! 0.133175f
cc_51 CTRL_2_BUF<2> VDD! 0.0590196f
cc_52 CTRL_2_BUF<2> XI0<1>/X1 0.0202366f
cc_53 CTRL_2_BUF<2> XI0<1>/Z_NEG 0.0270088f
cc_54 CTRL_2_BUF<2> XI4/Z_NEG 0.408473f
cc_55 CTRL_2_BUF<2> XI0<2>/X1 1.98567f
cc_56 CTRL_2_BUF<2> XI0<2>/Z_NEG 0.0171039f
cc_57 CTRL_2_BUF<2> XI11/Z_NEG 0.318211f
cc_58 ARITHMETIC_OUT<1> ALU_PRE<1> 0.0101137f
cc_59 ARITHMETIC_OUT<1> VSS! 0.00499882f
cc_60 ARITHMETIC_OUT<1> VDD! 0.0167587f
cc_61 ARITHMETIC_OUT<1> XI0<1>/X1 0.437839f
cc_62 ARITHMETIC_OUT<1> XI0<1>/Z_NEG 0.441849f
cc_63 LOGIC_OUT<1> ALU_PRE<1> 0.0133542f
cc_64 LOGIC_OUT<1> NET3<14> 7.05017e-19
cc_65 LOGIC_OUT<1> VSS! 0.00526763f
cc_66 LOGIC_OUT<1> VDD! 0.0204938f
cc_67 LOGIC_OUT<1> XI0<1>/X1 0.297816f
cc_68 LOGIC_OUT<1> XI0<1>/Z_NEG 0.00144576f
cc_69 CTRL_2_BUF<3> ARITHMETIC_OUT<2> 0.00797999f
cc_70 CTRL_2_BUF<3> LOGIC_OUT<2> 0.00676047f
cc_71 CTRL_2_BUF<3> CTRL_2_BUF<4> 0.0279411f
cc_72 CTRL_2_BUF<3> ARITHMETIC_OUT<3> 0.0395978f
cc_73 CTRL_2_BUF<3> LOGIC_OUT<3> 0.387056f
cc_74 CTRL_2_BUF<3> ALU_PRE<2> 0.0649196f
cc_75 CTRL_2_BUF<3> NET3<13> 0.0245011f
cc_76 CTRL_2_BUF<3> ALU_OUT<2> 0.0202126f
cc_77 CTRL_2_BUF<3> ALU_PRE<3> 0.11927f
cc_78 CTRL_2_BUF<3> NET3<12> 3.98891e-19
cc_79 CTRL_2_BUF<3> VSS! 0.133175f
cc_80 CTRL_2_BUF<3> VDD! 0.0590196f
cc_81 CTRL_2_BUF<3> XI0<3>/X1 1.98567f
cc_82 CTRL_2_BUF<3> XI0<3>/Z_NEG 0.0171039f
cc_83 CTRL_2_BUF<3> XI12/Z_NEG 0.318211f
cc_84 CTRL_2_BUF<3> XI0<2>/X1 0.0202366f
cc_85 CTRL_2_BUF<3> XI0<2>/Z_NEG 0.0270088f
cc_86 CTRL_2_BUF<3> XI11/Z_NEG 0.408473f
cc_87 ARITHMETIC_OUT<2> ALU_PRE<2> 0.0101137f
cc_88 ARITHMETIC_OUT<2> VSS! 0.00499882f
cc_89 ARITHMETIC_OUT<2> VDD! 0.0167587f
cc_90 ARITHMETIC_OUT<2> XI0<2>/X1 0.437839f
cc_91 ARITHMETIC_OUT<2> XI0<2>/Z_NEG 0.441849f
cc_92 LOGIC_OUT<2> ALU_PRE<2> 0.0133542f
cc_93 LOGIC_OUT<2> NET3<13> 7.05017e-19
cc_94 LOGIC_OUT<2> VSS! 0.00526763f
cc_95 LOGIC_OUT<2> VDD! 0.0204938f
cc_96 LOGIC_OUT<2> XI0<2>/X1 0.297816f
cc_97 LOGIC_OUT<2> XI0<2>/Z_NEG 0.00144576f
cc_98 CTRL_2_BUF<4> ARITHMETIC_OUT<3> 0.00797999f
cc_99 CTRL_2_BUF<4> LOGIC_OUT<3> 0.00676047f
cc_100 CTRL_2_BUF<4> CTRL_2_BUF<5> 0.0279411f
cc_101 CTRL_2_BUF<4> ARITHMETIC_OUT<4> 0.0395978f
cc_102 CTRL_2_BUF<4> LOGIC_OUT<4> 0.387056f
cc_103 CTRL_2_BUF<4> ALU_PRE<3> 0.0649196f
cc_104 CTRL_2_BUF<4> NET3<12> 0.0245011f
cc_105 CTRL_2_BUF<4> ALU_OUT<3> 0.0202126f
cc_106 CTRL_2_BUF<4> ALU_PRE<4> 0.11927f
cc_107 CTRL_2_BUF<4> NET3<11> 3.98891e-19
cc_108 CTRL_2_BUF<4> VSS! 0.133175f
cc_109 CTRL_2_BUF<4> VDD! 0.0590196f
cc_110 CTRL_2_BUF<4> XI0<3>/X1 0.0202366f
cc_111 CTRL_2_BUF<4> XI0<3>/Z_NEG 0.0270088f
cc_112 CTRL_2_BUF<4> XI12/Z_NEG 0.408473f
cc_113 CTRL_2_BUF<4> XI0<4>/X1 1.98567f
cc_114 CTRL_2_BUF<4> XI0<4>/Z_NEG 0.0171039f
cc_115 CTRL_2_BUF<4> XI19/Z_NEG 0.318211f
cc_116 ARITHMETIC_OUT<3> ALU_PRE<3> 0.0101137f
cc_117 ARITHMETIC_OUT<3> VSS! 0.00499882f
cc_118 ARITHMETIC_OUT<3> VDD! 0.0167587f
cc_119 ARITHMETIC_OUT<3> XI0<3>/X1 0.437839f
cc_120 ARITHMETIC_OUT<3> XI0<3>/Z_NEG 0.441849f
cc_121 LOGIC_OUT<3> ALU_PRE<3> 0.0133542f
cc_122 LOGIC_OUT<3> NET3<12> 7.05017e-19
cc_123 LOGIC_OUT<3> VSS! 0.00526763f
cc_124 LOGIC_OUT<3> VDD! 0.0204938f
cc_125 LOGIC_OUT<3> XI0<3>/X1 0.297816f
cc_126 LOGIC_OUT<3> XI0<3>/Z_NEG 0.00144576f
cc_127 CTRL_2_BUF<5> ARITHMETIC_OUT<4> 0.00797999f
cc_128 CTRL_2_BUF<5> LOGIC_OUT<4> 0.00676047f
cc_129 CTRL_2_BUF<5> CTRL_2_BUF<6> 0.0279411f
cc_130 CTRL_2_BUF<5> ARITHMETIC_OUT<5> 0.0395978f
cc_131 CTRL_2_BUF<5> LOGIC_OUT<5> 0.387056f
cc_132 CTRL_2_BUF<5> ALU_PRE<4> 0.0649196f
cc_133 CTRL_2_BUF<5> NET3<11> 0.0245011f
cc_134 CTRL_2_BUF<5> ALU_OUT<4> 0.0202126f
cc_135 CTRL_2_BUF<5> ALU_PRE<5> 0.11927f
cc_136 CTRL_2_BUF<5> NET3<10> 3.98891e-19
cc_137 CTRL_2_BUF<5> VSS! 0.133175f
cc_138 CTRL_2_BUF<5> VDD! 0.0590196f
cc_139 CTRL_2_BUF<5> XI0<5>/X1 1.98567f
cc_140 CTRL_2_BUF<5> XI0<5>/Z_NEG 0.0171039f
cc_141 CTRL_2_BUF<5> XI18/Z_NEG 0.318211f
cc_142 CTRL_2_BUF<5> XI0<4>/X1 0.0202366f
cc_143 CTRL_2_BUF<5> XI0<4>/Z_NEG 0.0270088f
cc_144 CTRL_2_BUF<5> XI19/Z_NEG 0.408473f
cc_145 ARITHMETIC_OUT<4> ALU_PRE<4> 0.0101137f
cc_146 ARITHMETIC_OUT<4> VSS! 0.00499882f
cc_147 ARITHMETIC_OUT<4> VDD! 0.0167587f
cc_148 ARITHMETIC_OUT<4> XI0<4>/X1 0.437839f
cc_149 ARITHMETIC_OUT<4> XI0<4>/Z_NEG 0.441849f
cc_150 LOGIC_OUT<4> ALU_PRE<4> 0.0133542f
cc_151 LOGIC_OUT<4> NET3<11> 7.05017e-19
cc_152 LOGIC_OUT<4> VSS! 0.00526763f
cc_153 LOGIC_OUT<4> VDD! 0.0204938f
cc_154 LOGIC_OUT<4> XI0<4>/X1 0.297816f
cc_155 LOGIC_OUT<4> XI0<4>/Z_NEG 0.00144576f
cc_156 CTRL_2_BUF<6> ARITHMETIC_OUT<5> 0.00797999f
cc_157 CTRL_2_BUF<6> LOGIC_OUT<5> 0.00676047f
cc_158 CTRL_2_BUF<6> CTRL_2_BUF<7> 0.0279411f
cc_159 CTRL_2_BUF<6> ARITHMETIC_OUT<6> 0.0395978f
cc_160 CTRL_2_BUF<6> LOGIC_OUT<6> 0.387056f
cc_161 CTRL_2_BUF<6> ALU_PRE<5> 0.0649196f
cc_162 CTRL_2_BUF<6> NET3<10> 0.0245011f
cc_163 CTRL_2_BUF<6> ALU_OUT<5> 0.0202126f
cc_164 CTRL_2_BUF<6> ALU_PRE<6> 0.11927f
cc_165 CTRL_2_BUF<6> NET3<9> 3.98891e-19
cc_166 CTRL_2_BUF<6> VSS! 0.133175f
cc_167 CTRL_2_BUF<6> VDD! 0.0590196f
cc_168 CTRL_2_BUF<6> XI0<5>/X1 0.0202366f
cc_169 CTRL_2_BUF<6> XI0<5>/Z_NEG 0.0270088f
cc_170 CTRL_2_BUF<6> XI18/Z_NEG 0.408473f
cc_171 CTRL_2_BUF<6> XI0<6>/X1 1.98567f
cc_172 CTRL_2_BUF<6> XI0<6>/Z_NEG 0.0171039f
cc_173 CTRL_2_BUF<6> XI17/Z_NEG 0.318211f
cc_174 ARITHMETIC_OUT<5> ALU_PRE<5> 0.0101137f
cc_175 ARITHMETIC_OUT<5> VSS! 0.00499882f
cc_176 ARITHMETIC_OUT<5> VDD! 0.0167587f
cc_177 ARITHMETIC_OUT<5> XI0<5>/X1 0.437839f
cc_178 ARITHMETIC_OUT<5> XI0<5>/Z_NEG 0.441849f
cc_179 LOGIC_OUT<5> ALU_PRE<5> 0.0133542f
cc_180 LOGIC_OUT<5> NET3<10> 7.05017e-19
cc_181 LOGIC_OUT<5> VSS! 0.00526763f
cc_182 LOGIC_OUT<5> VDD! 0.0204938f
cc_183 LOGIC_OUT<5> XI0<5>/X1 0.297816f
cc_184 LOGIC_OUT<5> XI0<5>/Z_NEG 0.00144576f
cc_185 CTRL_2_BUF<7> ARITHMETIC_OUT<6> 0.00797999f
cc_186 CTRL_2_BUF<7> LOGIC_OUT<6> 0.00676047f
cc_187 CTRL_2_BUF<7> CTRL_2_BUF<8> 0.0279411f
cc_188 CTRL_2_BUF<7> ARITHMETIC_OUT<7> 0.0395978f
cc_189 CTRL_2_BUF<7> LOGIC_OUT<7> 0.387056f
cc_190 CTRL_2_BUF<7> ALU_PRE<6> 0.0649196f
cc_191 CTRL_2_BUF<7> NET3<9> 0.0245011f
cc_192 CTRL_2_BUF<7> ALU_OUT<6> 0.0202126f
cc_193 CTRL_2_BUF<7> ALU_PRE<7> 0.11927f
cc_194 CTRL_2_BUF<7> NET3<8> 3.98891e-19
cc_195 CTRL_2_BUF<7> VSS! 0.133175f
cc_196 CTRL_2_BUF<7> VDD! 0.0590196f
cc_197 CTRL_2_BUF<7> XI0<7>/X1 1.98567f
cc_198 CTRL_2_BUF<7> XI0<7>/Z_NEG 0.0171039f
cc_199 CTRL_2_BUF<7> XI16/Z_NEG 0.318211f
cc_200 CTRL_2_BUF<7> XI0<6>/X1 0.0202366f
cc_201 CTRL_2_BUF<7> XI0<6>/Z_NEG 0.0270088f
cc_202 CTRL_2_BUF<7> XI17/Z_NEG 0.408473f
cc_203 ARITHMETIC_OUT<6> ALU_PRE<6> 0.0101137f
cc_204 ARITHMETIC_OUT<6> VSS! 0.00499882f
cc_205 ARITHMETIC_OUT<6> VDD! 0.0167587f
cc_206 ARITHMETIC_OUT<6> XI0<6>/X1 0.437839f
cc_207 ARITHMETIC_OUT<6> XI0<6>/Z_NEG 0.441849f
cc_208 LOGIC_OUT<6> ALU_PRE<6> 0.0133542f
cc_209 LOGIC_OUT<6> NET3<9> 7.05017e-19
cc_210 LOGIC_OUT<6> VSS! 0.00526763f
cc_211 LOGIC_OUT<6> VDD! 0.0204938f
cc_212 LOGIC_OUT<6> XI0<6>/X1 0.297816f
cc_213 LOGIC_OUT<6> XI0<6>/Z_NEG 0.00144576f
cc_214 CTRL_2_BUF<8> ARITHMETIC_OUT<7> 0.00797999f
cc_215 CTRL_2_BUF<8> LOGIC_OUT<7> 0.00676047f
cc_216 CTRL_2_BUF<8> CTRL_2_BUF<9> 0.0279411f
cc_217 CTRL_2_BUF<8> ARITHMETIC_OUT<8> 0.0395978f
cc_218 CTRL_2_BUF<8> LOGIC_OUT<8> 0.387056f
cc_219 CTRL_2_BUF<8> ALU_PRE<7> 0.0649196f
cc_220 CTRL_2_BUF<8> NET3<8> 0.0245011f
cc_221 CTRL_2_BUF<8> ALU_OUT<7> 0.0202126f
cc_222 CTRL_2_BUF<8> ALU_PRE<8> 0.11927f
cc_223 CTRL_2_BUF<8> NET3<7> 3.98891e-19
cc_224 CTRL_2_BUF<8> VSS! 0.133175f
cc_225 CTRL_2_BUF<8> VDD! 0.0590196f
cc_226 CTRL_2_BUF<8> XI0<7>/X1 0.0202366f
cc_227 CTRL_2_BUF<8> XI0<7>/Z_NEG 0.0270088f
cc_228 CTRL_2_BUF<8> XI16/Z_NEG 0.408473f
cc_229 CTRL_2_BUF<8> XI0<8>/X1 1.98567f
cc_230 CTRL_2_BUF<8> XI0<8>/Z_NEG 0.0171039f
cc_231 CTRL_2_BUF<8> XI20/Z_NEG 0.318211f
cc_232 ARITHMETIC_OUT<7> ALU_PRE<7> 0.0101137f
cc_233 ARITHMETIC_OUT<7> VSS! 0.00499882f
cc_234 ARITHMETIC_OUT<7> VDD! 0.0167587f
cc_235 ARITHMETIC_OUT<7> XI0<7>/X1 0.437839f
cc_236 ARITHMETIC_OUT<7> XI0<7>/Z_NEG 0.441849f
cc_237 LOGIC_OUT<7> ALU_PRE<7> 0.0133542f
cc_238 LOGIC_OUT<7> NET3<8> 7.05017e-19
cc_239 LOGIC_OUT<7> VSS! 0.00526763f
cc_240 LOGIC_OUT<7> VDD! 0.0204938f
cc_241 LOGIC_OUT<7> XI0<7>/X1 0.297816f
cc_242 LOGIC_OUT<7> XI0<7>/Z_NEG 0.00144576f
cc_243 CTRL_2_BUF<9> ARITHMETIC_OUT<8> 0.00797999f
cc_244 CTRL_2_BUF<9> LOGIC_OUT<8> 0.00676047f
cc_245 CTRL_2_BUF<9> CTRL_2_BUF<10> 0.0279411f
cc_246 CTRL_2_BUF<9> ARITHMETIC_OUT<9> 0.0395978f
cc_247 CTRL_2_BUF<9> LOGIC_OUT<9> 0.387056f
cc_248 CTRL_2_BUF<9> ALU_PRE<8> 0.0649196f
cc_249 CTRL_2_BUF<9> NET3<7> 0.0245011f
cc_250 CTRL_2_BUF<9> ALU_OUT<8> 0.0202126f
cc_251 CTRL_2_BUF<9> ALU_PRE<9> 0.11927f
cc_252 CTRL_2_BUF<9> NET3<6> 3.98891e-19
cc_253 CTRL_2_BUF<9> VSS! 0.133175f
cc_254 CTRL_2_BUF<9> VDD! 0.0590196f
cc_255 CTRL_2_BUF<9> XI0<9>/X1 1.98567f
cc_256 CTRL_2_BUF<9> XI0<9>/Z_NEG 0.0171039f
cc_257 CTRL_2_BUF<9> XI21/Z_NEG 0.318211f
cc_258 CTRL_2_BUF<9> XI0<8>/X1 0.0202366f
cc_259 CTRL_2_BUF<9> XI0<8>/Z_NEG 0.0270088f
cc_260 CTRL_2_BUF<9> XI20/Z_NEG 0.408473f
cc_261 ARITHMETIC_OUT<8> ALU_PRE<8> 0.0101137f
cc_262 ARITHMETIC_OUT<8> VSS! 0.00499882f
cc_263 ARITHMETIC_OUT<8> VDD! 0.0167587f
cc_264 ARITHMETIC_OUT<8> XI0<8>/X1 0.437839f
cc_265 ARITHMETIC_OUT<8> XI0<8>/Z_NEG 0.441849f
cc_266 LOGIC_OUT<8> ALU_PRE<8> 0.0133542f
cc_267 LOGIC_OUT<8> NET3<7> 7.05017e-19
cc_268 LOGIC_OUT<8> VSS! 0.00526763f
cc_269 LOGIC_OUT<8> VDD! 0.0204938f
cc_270 LOGIC_OUT<8> XI0<8>/X1 0.297816f
cc_271 LOGIC_OUT<8> XI0<8>/Z_NEG 0.00144576f
cc_272 CTRL_2_BUF<10> ARITHMETIC_OUT<9> 0.00797999f
cc_273 CTRL_2_BUF<10> LOGIC_OUT<9> 0.00676047f
cc_274 CTRL_2_BUF<10> CTRL_2_BUF<11> 0.0279411f
cc_275 CTRL_2_BUF<10> ARITHMETIC_OUT<10> 0.0395978f
cc_276 CTRL_2_BUF<10> LOGIC_OUT<10> 0.387056f
cc_277 CTRL_2_BUF<10> ALU_PRE<9> 0.0649196f
cc_278 CTRL_2_BUF<10> NET3<6> 0.0245011f
cc_279 CTRL_2_BUF<10> ALU_OUT<9> 0.0202126f
cc_280 CTRL_2_BUF<10> ALU_PRE<10> 0.11927f
cc_281 CTRL_2_BUF<10> NET3<5> 3.98891e-19
cc_282 CTRL_2_BUF<10> VSS! 0.133175f
cc_283 CTRL_2_BUF<10> VDD! 0.0590196f
cc_284 CTRL_2_BUF<10> XI0<9>/X1 0.0202366f
cc_285 CTRL_2_BUF<10> XI0<9>/Z_NEG 0.0270088f
cc_286 CTRL_2_BUF<10> XI21/Z_NEG 0.408473f
cc_287 CTRL_2_BUF<10> XI0<10>/X1 1.98567f
cc_288 CTRL_2_BUF<10> XI0<10>/Z_NEG 0.0171039f
cc_289 CTRL_2_BUF<10> XI22/Z_NEG 0.318211f
cc_290 ARITHMETIC_OUT<9> ALU_PRE<9> 0.0101137f
cc_291 ARITHMETIC_OUT<9> VSS! 0.00499882f
cc_292 ARITHMETIC_OUT<9> VDD! 0.0167587f
cc_293 ARITHMETIC_OUT<9> XI0<9>/X1 0.437839f
cc_294 ARITHMETIC_OUT<9> XI0<9>/Z_NEG 0.441849f
cc_295 LOGIC_OUT<9> ALU_PRE<9> 0.0133542f
cc_296 LOGIC_OUT<9> NET3<6> 7.05017e-19
cc_297 LOGIC_OUT<9> VSS! 0.00526763f
cc_298 LOGIC_OUT<9> VDD! 0.0204938f
cc_299 LOGIC_OUT<9> XI0<9>/X1 0.297816f
cc_300 LOGIC_OUT<9> XI0<9>/Z_NEG 0.00144576f
cc_301 CTRL_2_BUF<11> ARITHMETIC_OUT<10> 0.00797999f
cc_302 CTRL_2_BUF<11> LOGIC_OUT<10> 0.00676047f
cc_303 CTRL_2_BUF<11> CTRL_2_BUF<12> 0.0279411f
cc_304 CTRL_2_BUF<11> ARITHMETIC_OUT<11> 0.0395978f
cc_305 CTRL_2_BUF<11> LOGIC_OUT<11> 0.387056f
cc_306 CTRL_2_BUF<11> ALU_PRE<10> 0.0649196f
cc_307 CTRL_2_BUF<11> NET3<5> 0.0245011f
cc_308 CTRL_2_BUF<11> ALU_OUT<10> 0.0202126f
cc_309 CTRL_2_BUF<11> ALU_PRE<11> 0.11927f
cc_310 CTRL_2_BUF<11> NET3<4> 3.98891e-19
cc_311 CTRL_2_BUF<11> VSS! 0.133175f
cc_312 CTRL_2_BUF<11> VDD! 0.0590196f
cc_313 CTRL_2_BUF<11> XI0<11>/X1 1.98567f
cc_314 CTRL_2_BUF<11> XI0<11>/Z_NEG 0.0171039f
cc_315 CTRL_2_BUF<11> XI23/Z_NEG 0.318211f
cc_316 CTRL_2_BUF<11> XI0<10>/X1 0.0202366f
cc_317 CTRL_2_BUF<11> XI0<10>/Z_NEG 0.0270088f
cc_318 CTRL_2_BUF<11> XI22/Z_NEG 0.408473f
cc_319 ARITHMETIC_OUT<10> ALU_PRE<10> 0.0101137f
cc_320 ARITHMETIC_OUT<10> VSS! 0.00499882f
cc_321 ARITHMETIC_OUT<10> VDD! 0.0167587f
cc_322 ARITHMETIC_OUT<10> XI0<10>/X1 0.437839f
cc_323 ARITHMETIC_OUT<10> XI0<10>/Z_NEG 0.441849f
cc_324 LOGIC_OUT<10> ALU_PRE<10> 0.0133542f
cc_325 LOGIC_OUT<10> NET3<5> 7.05017e-19
cc_326 LOGIC_OUT<10> VSS! 0.00526763f
cc_327 LOGIC_OUT<10> VDD! 0.0204938f
cc_328 LOGIC_OUT<10> XI0<10>/X1 0.297816f
cc_329 LOGIC_OUT<10> XI0<10>/Z_NEG 0.00144576f
cc_330 CTRL_2_BUF<12> ARITHMETIC_OUT<11> 0.00797999f
cc_331 CTRL_2_BUF<12> LOGIC_OUT<11> 0.00676047f
cc_332 CTRL_2_BUF<12> CTRL_2_BUF<13> 0.0279411f
cc_333 CTRL_2_BUF<12> ARITHMETIC_OUT<12> 0.0395978f
cc_334 CTRL_2_BUF<12> LOGIC_OUT<12> 0.387056f
cc_335 CTRL_2_BUF<12> ALU_PRE<11> 0.0649196f
cc_336 CTRL_2_BUF<12> NET3<4> 0.0245011f
cc_337 CTRL_2_BUF<12> ALU_OUT<11> 0.0202126f
cc_338 CTRL_2_BUF<12> ALU_PRE<12> 0.11927f
cc_339 CTRL_2_BUF<12> NET3<3> 3.98891e-19
cc_340 CTRL_2_BUF<12> VSS! 0.133175f
cc_341 CTRL_2_BUF<12> VDD! 0.0590196f
cc_342 CTRL_2_BUF<12> XI0<11>/X1 0.0202366f
cc_343 CTRL_2_BUF<12> XI0<11>/Z_NEG 0.0270088f
cc_344 CTRL_2_BUF<12> XI23/Z_NEG 0.408473f
cc_345 CTRL_2_BUF<12> XI0<12>/X1 1.98567f
cc_346 CTRL_2_BUF<12> XI0<12>/Z_NEG 0.0171039f
cc_347 CTRL_2_BUF<12> XI27/Z_NEG 0.318211f
cc_348 ARITHMETIC_OUT<11> ALU_PRE<11> 0.0101137f
cc_349 ARITHMETIC_OUT<11> VSS! 0.00499882f
cc_350 ARITHMETIC_OUT<11> VDD! 0.0167587f
cc_351 ARITHMETIC_OUT<11> XI0<11>/X1 0.437839f
cc_352 ARITHMETIC_OUT<11> XI0<11>/Z_NEG 0.441849f
cc_353 LOGIC_OUT<11> ALU_PRE<11> 0.0133542f
cc_354 LOGIC_OUT<11> NET3<4> 7.05017e-19
cc_355 LOGIC_OUT<11> VSS! 0.00526763f
cc_356 LOGIC_OUT<11> VDD! 0.0204938f
cc_357 LOGIC_OUT<11> XI0<11>/X1 0.297816f
cc_358 LOGIC_OUT<11> XI0<11>/Z_NEG 0.00144576f
cc_359 CTRL_2_BUF<13> ARITHMETIC_OUT<12> 0.00797999f
cc_360 CTRL_2_BUF<13> LOGIC_OUT<12> 0.00676047f
cc_361 CTRL_2_BUF<13> CTRL_2_BUF<14> 0.0279411f
cc_362 CTRL_2_BUF<13> ARITHMETIC_OUT<13> 0.0395978f
cc_363 CTRL_2_BUF<13> LOGIC_OUT<13> 0.387056f
cc_364 CTRL_2_BUF<13> ALU_PRE<12> 0.0649196f
cc_365 CTRL_2_BUF<13> NET3<3> 0.0245011f
cc_366 CTRL_2_BUF<13> ALU_OUT<12> 0.0202126f
cc_367 CTRL_2_BUF<13> ALU_PRE<13> 0.11927f
cc_368 CTRL_2_BUF<13> NET3<2> 3.98891e-19
cc_369 CTRL_2_BUF<13> VSS! 0.133175f
cc_370 CTRL_2_BUF<13> VDD! 0.0590196f
cc_371 CTRL_2_BUF<13> XI0<13>/X1 1.98567f
cc_372 CTRL_2_BUF<13> XI0<13>/Z_NEG 0.0171039f
cc_373 CTRL_2_BUF<13> XI26/Z_NEG 0.318211f
cc_374 CTRL_2_BUF<13> XI0<12>/X1 0.0202366f
cc_375 CTRL_2_BUF<13> XI0<12>/Z_NEG 0.0270088f
cc_376 CTRL_2_BUF<13> XI27/Z_NEG 0.408473f
cc_377 ARITHMETIC_OUT<12> ALU_PRE<12> 0.0101137f
cc_378 ARITHMETIC_OUT<12> VSS! 0.00499882f
cc_379 ARITHMETIC_OUT<12> VDD! 0.0167587f
cc_380 ARITHMETIC_OUT<12> XI0<12>/X1 0.437839f
cc_381 ARITHMETIC_OUT<12> XI0<12>/Z_NEG 0.441849f
cc_382 LOGIC_OUT<12> ALU_PRE<12> 0.0133542f
cc_383 LOGIC_OUT<12> NET3<3> 7.05017e-19
cc_384 LOGIC_OUT<12> VSS! 0.00526763f
cc_385 LOGIC_OUT<12> VDD! 0.0204938f
cc_386 LOGIC_OUT<12> XI0<12>/X1 0.297816f
cc_387 LOGIC_OUT<12> XI0<12>/Z_NEG 0.00144576f
cc_388 CTRL_2_BUF<14> ARITHMETIC_OUT<13> 0.00797999f
cc_389 CTRL_2_BUF<14> LOGIC_OUT<13> 0.00676047f
cc_390 CTRL_2_BUF<14> CTRL_2_BUF<15> 0.0279411f
cc_391 CTRL_2_BUF<14> ARITHMETIC_OUT<14> 0.0395978f
cc_392 CTRL_2_BUF<14> LOGIC_OUT<14> 0.387056f
cc_393 CTRL_2_BUF<14> ALU_PRE<13> 0.0649196f
cc_394 CTRL_2_BUF<14> NET3<2> 0.0245011f
cc_395 CTRL_2_BUF<14> ALU_OUT<13> 0.0202126f
cc_396 CTRL_2_BUF<14> ALU_PRE<14> 0.11927f
cc_397 CTRL_2_BUF<14> NET3<1> 3.98891e-19
cc_398 CTRL_2_BUF<14> VSS! 0.133175f
cc_399 CTRL_2_BUF<14> VDD! 0.0590196f
cc_400 CTRL_2_BUF<14> XI0<14>/X1 1.98567f
cc_401 CTRL_2_BUF<14> XI0<14>/Z_NEG 0.0171039f
cc_402 CTRL_2_BUF<14> XI25/Z_NEG 0.318211f
cc_403 CTRL_2_BUF<14> XI0<13>/X1 0.0202366f
cc_404 CTRL_2_BUF<14> XI0<13>/Z_NEG 0.0270088f
cc_405 CTRL_2_BUF<14> XI26/Z_NEG 0.408473f
cc_406 ARITHMETIC_OUT<13> ALU_PRE<13> 0.0101137f
cc_407 ARITHMETIC_OUT<13> VSS! 0.00499882f
cc_408 ARITHMETIC_OUT<13> VDD! 0.0167587f
cc_409 ARITHMETIC_OUT<13> XI0<13>/X1 0.437839f
cc_410 ARITHMETIC_OUT<13> XI0<13>/Z_NEG 0.441849f
cc_411 LOGIC_OUT<13> ALU_PRE<13> 0.0133542f
cc_412 LOGIC_OUT<13> NET3<2> 7.05017e-19
cc_413 LOGIC_OUT<13> VSS! 0.00526763f
cc_414 LOGIC_OUT<13> VDD! 0.0204938f
cc_415 LOGIC_OUT<13> XI0<13>/X1 0.297816f
cc_416 LOGIC_OUT<13> XI0<13>/Z_NEG 0.00144576f
cc_417 CTRL_2_BUF<15> ARITHMETIC_OUT<14> 0.00797999f
cc_418 CTRL_2_BUF<15> LOGIC_OUT<14> 0.00676047f
cc_419 CTRL_2_BUF<15> CTRL<2> 0.0279411f
cc_420 CTRL_2_BUF<15> ARITHMETIC_OUT<15> 0.0395978f
cc_421 CTRL_2_BUF<15> LOGIC_OUT<15> 0.387056f
cc_422 CTRL_2_BUF<15> ALU_PRE<14> 0.0649196f
cc_423 CTRL_2_BUF<15> NET3<1> 0.0245011f
cc_424 CTRL_2_BUF<15> ALU_OUT<14> 0.0202126f
cc_425 CTRL_2_BUF<15> ALU_PRE<15> 0.11927f
cc_426 CTRL_2_BUF<15> NET3<0> 3.98891e-19
cc_427 CTRL_2_BUF<15> VSS! 0.133175f
cc_428 CTRL_2_BUF<15> VDD! 0.0590196f
cc_429 CTRL_2_BUF<15> XI0<15>/X1 1.98567f
cc_430 CTRL_2_BUF<15> XI0<15>/Z_NEG 0.0171039f
cc_431 CTRL_2_BUF<15> XI24/Z_NEG 0.318211f
cc_432 CTRL_2_BUF<15> XI0<14>/X1 0.0202366f
cc_433 CTRL_2_BUF<15> XI0<14>/Z_NEG 0.0270088f
cc_434 CTRL_2_BUF<15> XI25/Z_NEG 0.408473f
cc_435 ARITHMETIC_OUT<14> ALU_PRE<14> 0.0101137f
cc_436 ARITHMETIC_OUT<14> VSS! 0.00499882f
cc_437 ARITHMETIC_OUT<14> VDD! 0.0167587f
cc_438 ARITHMETIC_OUT<14> XI0<14>/X1 0.437839f
cc_439 ARITHMETIC_OUT<14> XI0<14>/Z_NEG 0.441849f
cc_440 LOGIC_OUT<14> ALU_PRE<14> 0.0133542f
cc_441 LOGIC_OUT<14> NET3<1> 7.05017e-19
cc_442 LOGIC_OUT<14> VSS! 0.00526763f
cc_443 LOGIC_OUT<14> VDD! 0.0204938f
cc_444 LOGIC_OUT<14> XI0<14>/X1 0.297816f
cc_445 LOGIC_OUT<14> XI0<14>/Z_NEG 0.00144576f
cc_446 CTRL<2> ARITHMETIC_OUT<15> 0.00797999f
cc_447 CTRL<2> LOGIC_OUT<15> 0.00676047f
cc_448 CTRL<2> ALU_PRE<15> 0.0649196f
cc_449 CTRL<2> NET3<0> 0.0244274f
cc_450 CTRL<2> ALU_OUT<15> 0.0190179f
cc_451 CTRL<2> VSS! 0.0569678f
cc_452 CTRL<2> VDD! 0.0059919f
cc_453 CTRL<2> XI0<15>/X1 0.0202366f
cc_454 CTRL<2> XI0<15>/Z_NEG 0.0270088f
cc_455 CTRL<2> XI24/Z_NEG 0.408473f
cc_456 ARITHMETIC_OUT<15> ALU_PRE<15> 0.0101137f
cc_457 ARITHMETIC_OUT<15> VSS! 0.00499882f
cc_458 ARITHMETIC_OUT<15> VDD! 0.0167587f
cc_459 ARITHMETIC_OUT<15> XI0<15>/X1 0.437839f
cc_460 ARITHMETIC_OUT<15> XI0<15>/Z_NEG 0.441849f
cc_461 LOGIC_OUT<15> ALU_PRE<15> 0.0133542f
cc_462 LOGIC_OUT<15> NET3<0> 7.05017e-19
cc_463 LOGIC_OUT<15> VSS! 0.00526763f
cc_464 LOGIC_OUT<15> VDD! 0.0204938f
cc_465 LOGIC_OUT<15> XI0<15>/X1 0.297816f
cc_466 LOGIC_OUT<15> XI0<15>/Z_NEG 0.00144576f
cc_467 ALU_PRE<0> NET3<15> 0.0974111f
cc_468 ALU_PRE<0> VSS! 0.0323896f
cc_469 ALU_PRE<0> VDD! 0.0575609f
cc_470 ALU_PRE<0> XI0<0>/X1 0.0211247f
cc_471 ALU_PRE<0> XI0<0>/Z_NEG 0.496461f
cc_472 ALU_PRE<0> XI3/Z_NEG 0.111678f
cc_473 NET3<15> ALU_OUT<0> 0.116846f
cc_474 NET3<15> VSS! 0.188348f
cc_475 NET3<15> VDD! 0.104348f
cc_476 NET3<15> XI0<0>/X1 0.00286433f
cc_477 ALU_OUT<0> VSS! 0.218498f
cc_478 ALU_OUT<0> VDD! 0.0260217f
cc_479 ALU_PRE<1> NET3<14> 0.0974111f
cc_480 ALU_PRE<1> VSS! 0.0323896f
cc_481 ALU_PRE<1> VDD! 0.0575609f
cc_482 ALU_PRE<1> XI0<1>/X1 0.0211247f
cc_483 ALU_PRE<1> XI0<1>/Z_NEG 0.496461f
cc_484 ALU_PRE<1> XI4/Z_NEG 0.111678f
cc_485 NET3<14> ALU_OUT<1> 0.116846f
cc_486 NET3<14> VSS! 0.188348f
cc_487 NET3<14> VDD! 0.104348f
cc_488 NET3<14> XI0<1>/X1 0.00286433f
cc_489 ALU_OUT<1> VSS! 0.218498f
cc_490 ALU_OUT<1> VDD! 0.0260217f
cc_491 ALU_PRE<2> NET3<13> 0.0974111f
cc_492 ALU_PRE<2> VSS! 0.0323896f
cc_493 ALU_PRE<2> VDD! 0.0575609f
cc_494 ALU_PRE<2> XI0<2>/X1 0.0211247f
cc_495 ALU_PRE<2> XI0<2>/Z_NEG 0.496461f
cc_496 ALU_PRE<2> XI11/Z_NEG 0.111678f
cc_497 NET3<13> ALU_OUT<2> 0.116846f
cc_498 NET3<13> VSS! 0.188348f
cc_499 NET3<13> VDD! 0.104348f
cc_500 NET3<13> XI0<2>/X1 0.00286433f
cc_501 ALU_OUT<2> VSS! 0.218498f
cc_502 ALU_OUT<2> VDD! 0.0260217f
cc_503 ALU_PRE<3> NET3<12> 0.0974111f
cc_504 ALU_PRE<3> VSS! 0.0323896f
cc_505 ALU_PRE<3> VDD! 0.0575609f
cc_506 ALU_PRE<3> XI0<3>/X1 0.0211247f
cc_507 ALU_PRE<3> XI0<3>/Z_NEG 0.496461f
cc_508 ALU_PRE<3> XI12/Z_NEG 0.111678f
cc_509 NET3<12> ALU_OUT<3> 0.116846f
cc_510 NET3<12> VSS! 0.188348f
cc_511 NET3<12> VDD! 0.104348f
cc_512 NET3<12> XI0<3>/X1 0.00286433f
cc_513 ALU_OUT<3> VSS! 0.218498f
cc_514 ALU_OUT<3> VDD! 0.0260217f
cc_515 ALU_PRE<4> NET3<11> 0.0974111f
cc_516 ALU_PRE<4> VSS! 0.0323896f
cc_517 ALU_PRE<4> VDD! 0.0575609f
cc_518 ALU_PRE<4> XI0<4>/X1 0.0211247f
cc_519 ALU_PRE<4> XI0<4>/Z_NEG 0.496461f
cc_520 ALU_PRE<4> XI19/Z_NEG 0.111678f
cc_521 NET3<11> ALU_OUT<4> 0.116846f
cc_522 NET3<11> VSS! 0.188348f
cc_523 NET3<11> VDD! 0.104348f
cc_524 NET3<11> XI0<4>/X1 0.00286433f
cc_525 ALU_OUT<4> VSS! 0.218498f
cc_526 ALU_OUT<4> VDD! 0.0260217f
cc_527 ALU_PRE<5> NET3<10> 0.0974111f
cc_528 ALU_PRE<5> VSS! 0.0323896f
cc_529 ALU_PRE<5> VDD! 0.0575609f
cc_530 ALU_PRE<5> XI0<5>/X1 0.0211247f
cc_531 ALU_PRE<5> XI0<5>/Z_NEG 0.496461f
cc_532 ALU_PRE<5> XI18/Z_NEG 0.111678f
cc_533 NET3<10> ALU_OUT<5> 0.116846f
cc_534 NET3<10> VSS! 0.188348f
cc_535 NET3<10> VDD! 0.104348f
cc_536 NET3<10> XI0<5>/X1 0.00286433f
cc_537 ALU_OUT<5> VSS! 0.218498f
cc_538 ALU_OUT<5> VDD! 0.0260217f
cc_539 ALU_PRE<6> NET3<9> 0.0974111f
cc_540 ALU_PRE<6> VSS! 0.0323896f
cc_541 ALU_PRE<6> VDD! 0.0575609f
cc_542 ALU_PRE<6> XI0<6>/X1 0.0211247f
cc_543 ALU_PRE<6> XI0<6>/Z_NEG 0.496461f
cc_544 ALU_PRE<6> XI17/Z_NEG 0.111678f
cc_545 NET3<9> ALU_OUT<6> 0.116846f
cc_546 NET3<9> VSS! 0.188348f
cc_547 NET3<9> VDD! 0.104348f
cc_548 NET3<9> XI0<6>/X1 0.00286433f
cc_549 ALU_OUT<6> VSS! 0.218498f
cc_550 ALU_OUT<6> VDD! 0.0260217f
cc_551 ALU_PRE<7> NET3<8> 0.0974111f
cc_552 ALU_PRE<7> VSS! 0.0323896f
cc_553 ALU_PRE<7> VDD! 0.0575609f
cc_554 ALU_PRE<7> XI0<7>/X1 0.0211247f
cc_555 ALU_PRE<7> XI0<7>/Z_NEG 0.496461f
cc_556 ALU_PRE<7> XI16/Z_NEG 0.111678f
cc_557 NET3<8> ALU_OUT<7> 0.116846f
cc_558 NET3<8> VSS! 0.188348f
cc_559 NET3<8> VDD! 0.104348f
cc_560 NET3<8> XI0<7>/X1 0.00286433f
cc_561 ALU_OUT<7> VSS! 0.218498f
cc_562 ALU_OUT<7> VDD! 0.0260217f
cc_563 ALU_PRE<8> NET3<7> 0.0974111f
cc_564 ALU_PRE<8> VSS! 0.0323896f
cc_565 ALU_PRE<8> VDD! 0.0575609f
cc_566 ALU_PRE<8> XI0<8>/X1 0.0211247f
cc_567 ALU_PRE<8> XI0<8>/Z_NEG 0.496461f
cc_568 ALU_PRE<8> XI20/Z_NEG 0.111678f
cc_569 NET3<7> ALU_OUT<8> 0.116846f
cc_570 NET3<7> VSS! 0.188348f
cc_571 NET3<7> VDD! 0.104348f
cc_572 NET3<7> XI0<8>/X1 0.00286433f
cc_573 ALU_OUT<8> VSS! 0.218498f
cc_574 ALU_OUT<8> VDD! 0.0260217f
cc_575 ALU_PRE<9> NET3<6> 0.0974111f
cc_576 ALU_PRE<9> VSS! 0.0323896f
cc_577 ALU_PRE<9> VDD! 0.0575609f
cc_578 ALU_PRE<9> XI0<9>/X1 0.0211247f
cc_579 ALU_PRE<9> XI0<9>/Z_NEG 0.496461f
cc_580 ALU_PRE<9> XI21/Z_NEG 0.111678f
cc_581 NET3<6> ALU_OUT<9> 0.116846f
cc_582 NET3<6> VSS! 0.188348f
cc_583 NET3<6> VDD! 0.104348f
cc_584 NET3<6> XI0<9>/X1 0.00286433f
cc_585 ALU_OUT<9> VSS! 0.218498f
cc_586 ALU_OUT<9> VDD! 0.0260217f
cc_587 ALU_PRE<10> NET3<5> 0.0974111f
cc_588 ALU_PRE<10> VSS! 0.0323896f
cc_589 ALU_PRE<10> VDD! 0.0575609f
cc_590 ALU_PRE<10> XI0<10>/X1 0.0211247f
cc_591 ALU_PRE<10> XI0<10>/Z_NEG 0.496461f
cc_592 ALU_PRE<10> XI22/Z_NEG 0.111678f
cc_593 NET3<5> ALU_OUT<10> 0.116846f
cc_594 NET3<5> VSS! 0.188348f
cc_595 NET3<5> VDD! 0.104348f
cc_596 NET3<5> XI0<10>/X1 0.00286433f
cc_597 ALU_OUT<10> VSS! 0.218498f
cc_598 ALU_OUT<10> VDD! 0.0260217f
cc_599 ALU_PRE<11> NET3<4> 0.0974111f
cc_600 ALU_PRE<11> VSS! 0.0323896f
cc_601 ALU_PRE<11> VDD! 0.0575609f
cc_602 ALU_PRE<11> XI0<11>/X1 0.0211247f
cc_603 ALU_PRE<11> XI0<11>/Z_NEG 0.496461f
cc_604 ALU_PRE<11> XI23/Z_NEG 0.111678f
cc_605 NET3<4> ALU_OUT<11> 0.116846f
cc_606 NET3<4> VSS! 0.188348f
cc_607 NET3<4> VDD! 0.104348f
cc_608 NET3<4> XI0<11>/X1 0.00286433f
cc_609 ALU_OUT<11> VSS! 0.218498f
cc_610 ALU_OUT<11> VDD! 0.0260217f
cc_611 ALU_PRE<12> NET3<3> 0.0974111f
cc_612 ALU_PRE<12> VSS! 0.0323896f
cc_613 ALU_PRE<12> VDD! 0.0575609f
cc_614 ALU_PRE<12> XI0<12>/X1 0.0211247f
cc_615 ALU_PRE<12> XI0<12>/Z_NEG 0.496461f
cc_616 ALU_PRE<12> XI27/Z_NEG 0.111678f
cc_617 NET3<3> ALU_OUT<12> 0.116846f
cc_618 NET3<3> VSS! 0.188348f
cc_619 NET3<3> VDD! 0.104348f
cc_620 NET3<3> XI0<12>/X1 0.00286433f
cc_621 ALU_OUT<12> VSS! 0.218498f
cc_622 ALU_OUT<12> VDD! 0.0260217f
cc_623 ALU_PRE<13> NET3<2> 0.0974111f
cc_624 ALU_PRE<13> VSS! 0.0323896f
cc_625 ALU_PRE<13> VDD! 0.0575609f
cc_626 ALU_PRE<13> XI0<13>/X1 0.0211247f
cc_627 ALU_PRE<13> XI0<13>/Z_NEG 0.496461f
cc_628 ALU_PRE<13> XI26/Z_NEG 0.111678f
cc_629 NET3<2> ALU_OUT<13> 0.116846f
cc_630 NET3<2> VSS! 0.188348f
cc_631 NET3<2> VDD! 0.104348f
cc_632 NET3<2> XI0<13>/X1 0.00286433f
cc_633 ALU_OUT<13> VSS! 0.218498f
cc_634 ALU_OUT<13> VDD! 0.0260217f
cc_635 ALU_PRE<14> NET3<1> 0.0974111f
cc_636 ALU_PRE<14> VSS! 0.0323896f
cc_637 ALU_PRE<14> VDD! 0.0575609f
cc_638 ALU_PRE<14> XI0<14>/X1 0.0211247f
cc_639 ALU_PRE<14> XI0<14>/Z_NEG 0.496461f
cc_640 ALU_PRE<14> XI25/Z_NEG 0.111678f
cc_641 NET3<1> ALU_OUT<14> 0.116846f
cc_642 NET3<1> VSS! 0.188348f
cc_643 NET3<1> VDD! 0.104348f
cc_644 NET3<1> XI0<14>/X1 0.00286433f
cc_645 ALU_OUT<14> VSS! 0.218498f
cc_646 ALU_OUT<14> VDD! 0.0260217f
cc_647 ALU_PRE<15> NET3<0> 0.148182f
cc_648 ALU_PRE<15> ALU_OUT<15> 0.0261155f
cc_649 ALU_PRE<15> VSS! 0.0336415f
cc_650 ALU_PRE<15> VDD! 0.0660944f
cc_651 ALU_PRE<15> XI0<15>/X1 0.0211247f
cc_652 ALU_PRE<15> XI0<15>/Z_NEG 0.496461f
cc_653 ALU_PRE<15> XI24/Z_NEG 0.111678f
cc_654 NET3<0> ALU_OUT<15> 0.116097f
cc_655 NET3<0> VSS! 0.188047f
cc_656 NET3<0> VDD! 0.104014f
cc_657 NET3<0> XI0<15>/X1 0.00286433f
cc_658 ALU_OUT<15> VSS! 0.219145f
cc_659 ALU_OUT<15> VDD! 0.0259954f
cc_660 VSS! VDD! 0.0826546f
cc_661 VSS! XI0<15>/X1 0.154013f
cc_662 VSS! XI0<15>/Z_NEG 0.0328266f
cc_663 VSS! XI24/Z_NEG 0.0531738f
cc_664 VSS! XI0<14>/X1 0.154013f
cc_665 VSS! XI0<14>/Z_NEG 0.0328266f
cc_666 VSS! XI25/Z_NEG 0.0531738f
cc_667 VSS! XI0<1>/X1 0.154013f
cc_668 VSS! XI0<1>/Z_NEG 0.0328266f
cc_669 VSS! XI4/Z_NEG 0.0531738f
cc_670 VSS! XI0<0>/X1 0.154013f
cc_671 VSS! XI0<0>/Z_NEG 0.0328266f
cc_672 VSS! XI3/Z_NEG 0.0532045f
cc_673 VSS! XI0<3>/X1 0.154013f
cc_674 VSS! XI0<3>/Z_NEG 0.0328266f
cc_675 VSS! XI12/Z_NEG 0.0531738f
cc_676 VSS! XI0<2>/X1 0.154013f
cc_677 VSS! XI0<2>/Z_NEG 0.0328266f
cc_678 VSS! XI11/Z_NEG 0.0531738f
cc_679 VSS! XI0<5>/X1 0.154013f
cc_680 VSS! XI0<5>/Z_NEG 0.0328266f
cc_681 VSS! XI18/Z_NEG 0.0531738f
cc_682 VSS! XI0<4>/X1 0.154013f
cc_683 VSS! XI0<4>/Z_NEG 0.0328266f
cc_684 VSS! XI19/Z_NEG 0.0531738f
cc_685 VSS! XI0<7>/X1 0.154013f
cc_686 VSS! XI0<7>/Z_NEG 0.0328266f
cc_687 VSS! XI16/Z_NEG 0.0531738f
cc_688 VSS! XI0<6>/X1 0.154013f
cc_689 VSS! XI0<6>/Z_NEG 0.0328266f
cc_690 VSS! XI17/Z_NEG 0.0531738f
cc_691 VSS! XI0<9>/X1 0.154013f
cc_692 VSS! XI0<9>/Z_NEG 0.0328266f
cc_693 VSS! XI21/Z_NEG 0.0531738f
cc_694 VSS! XI0<8>/X1 0.154013f
cc_695 VSS! XI0<8>/Z_NEG 0.0328266f
cc_696 VSS! XI20/Z_NEG 0.0531738f
cc_697 VSS! XI0<11>/X1 0.154013f
cc_698 VSS! XI0<11>/Z_NEG 0.0328266f
cc_699 VSS! XI23/Z_NEG 0.0531738f
cc_700 VSS! XI0<10>/X1 0.154013f
cc_701 VSS! XI0<10>/Z_NEG 0.0328266f
cc_702 VSS! XI22/Z_NEG 0.0531738f
cc_703 VSS! XI0<13>/X1 0.154013f
cc_704 VSS! XI0<13>/Z_NEG 0.0328266f
cc_705 VSS! XI26/Z_NEG 0.0531738f
cc_706 VSS! XI0<12>/X1 0.154013f
cc_707 VSS! XI0<12>/Z_NEG 0.0328266f
cc_708 VSS! XI27/Z_NEG 0.0531738f
cc_709 VDD! XI0<15>/X1 0.0825265f
cc_710 VDD! XI0<15>/Z_NEG 0.0649177f
cc_711 VDD! XI24/Z_NEG 0.0548853f
cc_712 VDD! XI0<14>/X1 0.0825265f
cc_713 VDD! XI0<14>/Z_NEG 0.0649177f
cc_714 VDD! XI25/Z_NEG 0.0548853f
cc_715 VDD! XI0<1>/X1 0.0825265f
cc_716 VDD! XI0<1>/Z_NEG 0.0649177f
cc_717 VDD! XI4/Z_NEG 0.0548853f
cc_718 VDD! XI0<0>/X1 0.0825265f
cc_719 VDD! XI0<0>/Z_NEG 0.0649177f
cc_720 VDD! XI3/Z_NEG 0.0548853f
cc_721 VDD! XI0<3>/X1 0.0825265f
cc_722 VDD! XI0<3>/Z_NEG 0.0649177f
cc_723 VDD! XI12/Z_NEG 0.0548853f
cc_724 VDD! XI0<2>/X1 0.0825265f
cc_725 VDD! XI0<2>/Z_NEG 0.0649177f
cc_726 VDD! XI11/Z_NEG 0.0548853f
cc_727 VDD! XI0<5>/X1 0.0825265f
cc_728 VDD! XI0<5>/Z_NEG 0.0649177f
cc_729 VDD! XI18/Z_NEG 0.0548853f
cc_730 VDD! XI0<4>/X1 0.0825265f
cc_731 VDD! XI0<4>/Z_NEG 0.0649177f
cc_732 VDD! XI19/Z_NEG 0.0548853f
cc_733 VDD! XI0<7>/X1 0.0825265f
cc_734 VDD! XI0<7>/Z_NEG 0.0649177f
cc_735 VDD! XI16/Z_NEG 0.0548853f
cc_736 VDD! XI0<6>/X1 0.0825265f
cc_737 VDD! XI0<6>/Z_NEG 0.0649177f
cc_738 VDD! XI17/Z_NEG 0.0548853f
cc_739 VDD! XI0<9>/X1 0.0825265f
cc_740 VDD! XI0<9>/Z_NEG 0.0649177f
cc_741 VDD! XI21/Z_NEG 0.0548853f
cc_742 VDD! XI0<8>/X1 0.0825265f
cc_743 VDD! XI0<8>/Z_NEG 0.0649177f
cc_744 VDD! XI20/Z_NEG 0.0548853f
cc_745 VDD! XI0<11>/X1 0.0825265f
cc_746 VDD! XI0<11>/Z_NEG 0.0649177f
cc_747 VDD! XI23/Z_NEG 0.0548853f
cc_748 VDD! XI0<10>/X1 0.0825265f
cc_749 VDD! XI0<10>/Z_NEG 0.0649177f
cc_750 VDD! XI22/Z_NEG 0.0548853f
cc_751 VDD! XI0<13>/X1 0.0825265f
cc_752 VDD! XI0<13>/Z_NEG 0.0649177f
cc_753 VDD! XI26/Z_NEG 0.0548853f
cc_754 VDD! XI0<12>/X1 0.0825265f
cc_755 VDD! XI0<12>/Z_NEG 0.0649177f
cc_756 VDD! XI27/Z_NEG 0.0548853f
cc_757 XI0<15>/X1 XI0<15>/Z_NEG 0.151029f
cc_758 XI0<15>/Z_NEG XI24/Z_NEG 6.05329e-19
cc_759 XI0<14>/X1 XI0<14>/Z_NEG 0.151029f
cc_760 XI0<14>/Z_NEG XI25/Z_NEG 6.05329e-19
cc_761 XI0<1>/X1 XI0<1>/Z_NEG 0.151029f
cc_762 XI0<1>/Z_NEG XI4/Z_NEG 6.05329e-19
cc_763 XI0<0>/X1 XI0<0>/Z_NEG 0.151029f
cc_764 XI0<0>/Z_NEG XI3/Z_NEG 6.05329e-19
cc_765 XI0<3>/X1 XI0<3>/Z_NEG 0.151029f
cc_766 XI0<3>/Z_NEG XI12/Z_NEG 6.05329e-19
cc_767 XI0<2>/X1 XI0<2>/Z_NEG 0.151029f
cc_768 XI0<2>/Z_NEG XI11/Z_NEG 6.05329e-19
cc_769 XI0<5>/X1 XI0<5>/Z_NEG 0.151029f
cc_770 XI0<5>/Z_NEG XI18/Z_NEG 6.05329e-19
cc_771 XI0<4>/X1 XI0<4>/Z_NEG 0.151029f
cc_772 XI0<4>/Z_NEG XI19/Z_NEG 6.05329e-19
cc_773 XI0<7>/X1 XI0<7>/Z_NEG 0.151029f
cc_774 XI0<7>/Z_NEG XI16/Z_NEG 6.05329e-19
cc_775 XI0<6>/X1 XI0<6>/Z_NEG 0.151029f
cc_776 XI0<6>/Z_NEG XI17/Z_NEG 6.05329e-19
cc_777 XI0<9>/X1 XI0<9>/Z_NEG 0.151029f
cc_778 XI0<9>/Z_NEG XI21/Z_NEG 6.05329e-19
cc_779 XI0<8>/X1 XI0<8>/Z_NEG 0.151029f
cc_780 XI0<8>/Z_NEG XI20/Z_NEG 6.05329e-19
cc_781 XI0<11>/X1 XI0<11>/Z_NEG 0.151029f
cc_782 XI0<11>/Z_NEG XI23/Z_NEG 6.05329e-19
cc_783 XI0<10>/X1 XI0<10>/Z_NEG 0.151029f
cc_784 XI0<10>/Z_NEG XI22/Z_NEG 6.05329e-19
cc_785 XI0<13>/X1 XI0<13>/Z_NEG 0.151029f
cc_786 XI0<13>/Z_NEG XI26/Z_NEG 6.05329e-19
cc_787 XI0<12>/X1 XI0<12>/Z_NEG 0.151029f
cc_788 XI0<12>/Z_NEG XI27/Z_NEG 6.05329e-19
